import React, {Component} from "react";

class FormClient extends Component {
    
    state = {
        nouveauClient: ""
    }

    //Fonction fléchée qui récupère un événement en param
    handleSubmit = (event) => {
        event.preventDefault(); //quand tu récupère l'événement stop ce que tu dois faire par defaut en mode web,
                                //ne rafraichi que le contexte du navigateur et non la page
        const id = new Date().getTime();//Je récupère le temps en ms
        const nom = this.state.nouveauClient;
        this.props.onClientAdd({id, nom}); //Envoi d'un tableau d'id & de nom à la fonction handleAdd() dans 'index.js'
        this.setState({nouveauClient:""});
    }

    //Fonction qui permet de récupérer la valeur saisie dans le champs 'nouveau client'
    handleChange = (event) => {
        console.log(event.currentTarget.value);
        this.setState({nouveauClient: event.currentTarget.value})
      }


    render() {

        return <form onSubmit={this.handleSubmit}> 
                    <input  value={this.state.nouveauClient} 
                            type="text" 
                            onChange={this.handleChange} 
                            placeholder="Ajouter un client"/>
                    <button> Valider </button>
                </form>
    }
}

export default FormClient;



