import React from 'react';
import ReactDOM from 'react-dom';
import Client from "./Client";
import FormClient from "./FormClient";
import './index.css';


//Création de 'app' en tant que composant qui renvoi un retour --> élément HTML
class App extends React.Component{

  //Création d'un objet d'état qui contient des clients sous forme de liste
  state = {

    //définition de chaque client qui contient un id et un nom
    clients: [
      {id: 1, nom:"Stéphane Didier"},
    ],
    compteur : 1
  };

  //fonction qui permet de supprimer un élément par id
  handleDelete = (id) => {
    console.log(id);
    const clients = [...this.state.clients]; //récupération de la liste de clients //récupération de la liste de clients
    const index = clients.findIndex(function(client) {
      return client.id === id
    });
    clients.splice(index,1);
    this.setState({clients: clients, compteur: this.state.compteur -1}); //-1 au compteur
  }

  //Fonction qui est chargée d'ajouter un client 
  handleAdd = (client) => {
    const clients = [...this.state.clients]; //récupération de la liste de clients
    clients.push(client); //push d'un nouveau client
    this.setState({clients, compteur: this.state.compteur +1}); //on remet client dans le  state + compteur +=1
  }  


  render() {

    const title = "Liste des clients"

    return <div>
                <h1>{title} {this.state.compteur}</h1>
                <ul>
                    {this.state.clients.map(client => (<Client pClient={client} onDelete={this.handleDelete} />))}
                </ul>
                <FormClient onClientAdd={this.handleAdd} />
            </div>
  };
}


const rootElement = document.getElementById('root'); //creation var de type 'rootElement' qui est égale à id root

//demande de renvoi vers le DOM de React
ReactDOM.render(<App />, rootElement);

