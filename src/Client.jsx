import React, {Component} from "react";

/* 
COMPOSANT FONCTIONNEL 
Pas besoin de gestion d'état -> pas besoin de classe
*/

/* Création d'une fonction fléchée qui prends en params 'pClient et onDelete' et qui retourne l'élément <li></li> 
Si un seul type de retour, pas besoin de noter le 'return'
*/
const Client = ({pClient, onDelete}) => (
        //return
        <li>{pClient.nom} 
            <button onClick ={() => onDelete(pClient.id)} > x </button>
        </li>
    
)

export default Client;